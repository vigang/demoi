# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

import pymongo


# class MongoPipeline(object):
#     def __init__(self, mongo_uri, mongo_db):
#         self.mongo_uri = mongo_uri
#         self.mongo_db = mongo_db
#
#     @classmethod
#     def from_crawler(cls, crawler):
#         return cls(
#             mongo_uri=crawler.settings.get('MONGO_URI'),
#             mongo_db=crawler.settings.get('MONGO_DB')
#         )
#
#     def open_spider(self, spider):
#         self.client = pymongo.MongoClient(self.mongo_uri)
#         self.db = self.client[self.mongo_db]
#
#     def process_item(self, item, spider):
#         # self.db[item.collection].insert(dict(item))
#         # self.db[item.collection].update({'code': item["code"]}, {'$set': item}, True)
#         # if self.db[item.collection].find({"code": item["code"]}):
#         #     self.db[item.collection].update({'code': item["code"]}, {'$set': item})
#         # else:
#         self.db[item.collection].update({"code": item["code"]}, {"$set": dict(item)}, True)
#         print("保存数据库成功")
#         return item
#
#     def closs_spider(self, spider):
#         self.client.close()


class MongoPipeline(object):
    def __init__(self, mongo_uri, mongo_db):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            mongo_uri=crawler.settings.get('MONGO_URI'),
            mongo_db=crawler.settings.get('MONGO_DB')
        )

    def open_spider(self, spider):
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]

    def process_item(self, item, spider):
        # self.db[item.collection].insert(dict(item))
        # self.db[item.collection].update({'code': item["code"]}, {'$set': item}, True)
        # if self.db[item.collection].find({"name": item["name"]}):
        #     self.db[item.collection].update({'name': item["name"]}, {'$push': {"content": item["content"]}})
        # else:
        #     self.db[item.collection].insert(dict(item))
        self.db[item.collection].update({'name': item["name"]}, {'$set': dict(item)}, True)
        return item

    def closs_spider(self, spider):
        self.client.close()
