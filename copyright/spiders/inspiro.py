# -*- coding: utf-8 -*-
import json
import random
import time

import pymysql
import requests
import scrapy
import copy
from copyright.items import CopyrightItem
from aoyun.util import user_agent


class InspiroSpider(scrapy.Spider):
    """261895183"""
    name = 'inspiro'
    allowed_domains = ['publicdi.com']
    # start_urls = ['http://www.publicdi.com/api/copyright/soft/search/expression']
    url = "http://www.publicdi.com/api/copyright/soft/search/expression"
    data = {

        "page": "1",
        "sort_column": "-RELEVANCE"
    }
    total = 0
    headers = {
        "User-Agent": random.choice(user_agent.USER_AGENTS),
        "Referer": "http://www.publicdi.com"
    }

    def start_requests(self):
        with open("D:\\copyright\\copyright\\spiders\\company.txt", "r", errors='ignore') as f:

            self.total += 1
            companys = f.readlines()
            for company in companys:
                data = self.data
                self.total += 1
                data["express"] = "(著作权人 = ( {} ) ) ".format(company)
                print("第{}条数据:{}".format(self.total, company))
                yield scrapy.FormRequest(self.url, formdata=data, headers=self.headers, method="POST",
                                         meta={"company": copy.deepcopy(company), "data": copy.deepcopy(data)})

    def parse(self, response):
        # print("开始解析")
        company = response.meta["company"]
        data = response.meta["data"]

        contents = json.loads(response.body.decode())
        infos = contents.get("context").get("records")
        if infos:
            total = int(contents.get("total"))
            item = CopyrightItem()
            item["name"] = company
            item["content"] = []
            for info in infos:
                i = {}
                i["RN"] = info.get("RN")  # 登记号
                i["CTN"] = info.get("CTN")  # 分类号
                i["SWFN"] = info.get("SWFN")  # 软件全称
                i["SWV"] = info.get("SWV")  # 版本号

                i["SWSN"] = info.get("SWSN")  # 软件简称
                i["SWP"] = info.get("SWP")  # 著作权人
                i["PDF"] = info.get("PDF")  # 首次发表日期
                i["RD"] = info.get("RD")  # 登记日期
                item["content"].append(i)

            if total > 10:
                for i in range(total // 10):
                    page = i + 2
                    data["page"] = str(page)
                    datas = self.other_page(data)
                    for k in datas:
                        item["content"].append(k)
            yield item

    def other_page(self, data):
        proxys = self.get_random_proxy()
        proxy = random.choice(proxys)
        proxy = {"http:": proxy}

        resp = requests.post(self.url, data=data, headers=self.headers,proxies=proxy)
        contents = json.loads(resp.content.decode())
        print("this is request ip:", proxy)
        infos = contents.get("context").get("records")
        for info in infos:
            i = {}
            i["RN"] = info.get("RN")  # 登记号
            i["CTN"] = info.get("CTN")  # 分类号
            i["SWFN"] = info.get("SWFN")  # 软件全称
            i["SWV"] = info.get("SWV")  # 版本号

            i["SWSN"] = info.get("SWSN")  # 软件简称
            i["SWP"] = info.get("SWP")  # 著作权人
            i["PDF"] = info.get("PDF")  # 首次发表日期
            i["RD"] = info.get("RD")
            yield i

    def get_random_proxy(self):
        '''随机从文件中读取proxy'''
        conn = pymysql.connect(host="localhost", user="root", passwd="123456", db="ips", charset="utf8")
        cursor = conn.cursor()
        sql = "select * from ip  LIMIT 0,150"
        cursor.execute(sql)
        datas = cursor.fetchall()
        cursor.close()
        conn.close()
        ips = []
        for data in datas:
            ips.append('http://' + data[1])

        return ips
